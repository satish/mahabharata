# Dhyānaṡlōkāḥ

> ¹ṡrīḥ hariḥ ōṃ  
> ¹ṡrī vēdavyāsāya namaḥ  
> ¹ṡrīmatē rāmanujāya namaḥ  
> ²ṡrī gurubhyō namaḥ  

¹ G7; PPS begins with only "ṡrīḥ";
  "ṡrī" and "ṡrīḥ" are both valid nominative forms of "ṡrī"  
² T1

> ṡuklāmbaradharaṃ ¹viṣṇuṃ ṡaṡivarṇaṃ caturbhujam  
> prasannavadanaṃ dhyāyēt sarvavighnōpaṡāntayē /1/  

⁰ G2,6,7 CE@9 PPS@1  
¹ all; CE "dēvaṃ"

> jn̄ānānandamayaṃ dēvaṃ nirmalaṃ sphaṭikākṛtim  
> ādhāraṃ sarvavidyānāṃ hayagrīvam upāsmahē /2/  

⁰ G2 CE@10

> vyāsaṃ vasiṣṭhanaptāraṃ ṡaktēḥ pautram akalmaṣam  
> parāṡarātmajaṃ vandē ṡukatātaṃ tapōnidhim /3/  

⁰ G2 T1 CE@7 PPS@9

> vyāsāya viṣṇurūpāya vyāsarūpāya viṣṇavē  
> namō vai brahmanidhayē vāsiṣṭhāya namō namaḥ /4/  

⁰ G2 CE@11

> abhraṡyāmaḥ piṅgajaṭābaddhakalāpaḥ  
>  prāṃṡur daṇḍī kṛṣṇamṛgatvakparidhānaḥ  
> sākṣāl lōkān pāvayamānaḥ kavimukhyaḥ  
>  pārāṡaryaḥ parvasu rūpaṃ vivṛṇōtu /5/  

⁰ G2 T1 CE@13 PPS@10

> pārāṡaryavacassarōjam amalaṃ gītārthagandhōtkaṭaṃ  
>  nānākhyānakakēsaraṃ harikathāsaṃbōdhanābōdhitam  
> lōkē sajjanaṣaṭpadair aharahaḥ pēpīyamānaṃ mudā  
>  bhūyād bhāratapaṅkajaṃ kalimalapradhvaṃsi naḥ ṡrēyasē /6/  

⁰ G2 K3 CE@4 PPS@11

> dvaipāyanauṣṭhapuṭaniḥsṛtam apramēyaṃ  
>  puṇyaṃ pavitram atha pāpaharaṃ ṡivaṃ ca  
> yō bhārataṃ samadhigacchati vācyamānaṃ  
>  kiṃ tasya puṣkarajalair abhiṣēcanēna /7/  

⁰ G2 T1 CE 1.2.242

> dharmō vivardhati yudhiṣṭhirakīrtanēna  
>  pāpaṃ praṇaṡyati vṛkōdarakīrtanēna  
> ṡatrur vinaṡyati dhanaṃjayakīrtanēna  
>  mādrīsutau kathayatāṃ na bhavanti rōgāḥ /8/  

⁰ G6 CE@15 PPS@2

> bhāratādhyayanāt puṇyād api pādam adhīyataḥ  
> ṡraddadhānasya pūyantē sarvapāpāny aṡēṣataḥ /9/  

⁰ G2,6 CE 1.1.191 PPS@3

> namō dharmāya mahatē namaḥ kṛṣṇāya vēdhasē  
> brāhmaṇēbhyō namaskṛtvā dharmān vakṣyāmi ¹ṡāṡvatān /10/  

⁰ G2 T1 CE@12 PPS@12 Bhāg. 12.12.1  
¹ all; Bhāg. "sanātanān"

> nārāyaṇaṃ namaskṛtya naraṃ caiva narōttamam  
> dēvīṃ sarasvatīṃ ¹vyāsaṃ tatō jayam udīrayēt /11/  

⁰ T1 G7 CE 1.1.0  
¹ T1 G7; et al. "caiva"

Verses 1-2 glorify Lord Viṣṇu, verses 3-5 glorify Lord Vēdavyāsa and
verses 6-9 Mahābhārata itself. Verses 10-11 are spoken by Ṡūta muni
who is the main narrator of the epic.

None of the Malayalam mss. include above ṡlokas. Hence they're relegated
to this separate chapter. Instead, Ādi Parva begins with M3's CE@17.

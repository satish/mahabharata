# Ṡrī-viṣṇu-saharsanāma stōtra mahā-mantra  

A critical edition of Sri Vishnu Sahasranamam based on Malayalam mss. esp. M1-3 and often M4, when
in agreement with the rest of TG.

M1-4 -- Malayalam mss.  
T1-3 -- Telugu mss.  
G1-4 -- Grantha mss.  
Dn -- Nīlakaṇṭha's Devanagari mss. ("vulgate" or "Gita Press")  
Cp -- Parāṡara Bhaṭṭa's commentary (Rāmanuja/Viṡiṣṭādvaita)  
Cv -- Vādirāja's Lakṣālaṅkāra commentary (Madhva/Dvaita)  
marg. -- marginal note  
alt. -- alternative reading (pāṭhāntara)

## Pūrva-pīṭhikā

>    vaiṡampāyana uvāca  
> ṡrutvā dharmān aṡēṣēṇa pāvanāni ca sarvaṡaḥ  
> yudhiṣṭhiraḥ ṡāntanavaṃ punar ēvābhyabhāṣata /1/  

> kim ēkaṃ daivataṃ lōkē kiṃ vāpy ēkaṃ parāyaṇam  
> stuvantaḥ kaṃ kam arcantaḥ prāpnuyur mānavāḥ ṡubham /2/  

> kō dharmaḥ sarvadharmāṇāṃ bhavataḥ paramō mataḥ  
> kiṃ japan mucyatē jantur janmasaṃsārabandhanāt /3/  

>    bhīṣma uvāca  
> jagatprabhuṃ dēvadēvam anantaṃ puruṣōttamam  
> stuvan nāmasahasrēṇa puruṣaḥ satatōtthitaḥ /4/  

> tam ēva cārcayan nityaṃ bhaktyā puruṣam avyayam  
> dhyāyan stuvan namasyaṃṡ ca yajamānas tam ēva ca /5/  

> anādinidhanaṃ viṣṇuṃ sarvalōkamahēṡvaram  
> lōkādhyakṣaṃ stuvan nityaṃ sarvaduḥkhātigō bhavēt /6/  

> brahmaṇyaṃ sarvadharmajn̄aṃ lōkānāṃ kīrtivardhanam  
> lōkanāthaṃ mahad bhūtaṃ sarvabhūtabhavōdbhavam /7/  

> ēṣa mē sarvadharmāṇāṃ dharmō’dhikatamō mataḥ  
> yad bhaktyā puṇḍarīkākṣaṃ stavair arcēn naraḥ sadā /8/  

> paramaṃ yō mahat tējaḥ paramaṃ yō mahat tapaḥ  
> paramaṃ yō mahad brahma paramaṃ yaḥ parāyaṇam /9/  

> pavitrāṇāṃ pavitraṃ yō maṅgaḻānāṃ ca maṅgaḻam  
> daivataṃ dēvatānāṃ ca bhūtānāṃ yō’vyayaḥ pitā /10/  

> yataḥ sarvāṇi bhūtāni bhavanty ādiyugāgamē  
> yasmiṃṡ ca praḻayaṃ yānti punar ēva yugakṣayē /11/  

> tasya lōkapradhānasya jagannāthasya bhūpatē  
> viṣṇōr nāmasahasraṃ mē ṡṛṇu pāpabhayāpaham /12/  

> yāni nāmāni gauṇāni vikhyātāni mahātmanaḥ  
> ṛṣibhiḥ parigītāni tāni vakṣyāmi bhūtayē /13/  

After /13/, M1-3 T G D10 in. CE@632 as /14/-/15/.
Only Northern mss. insert "...kīlakaṃ ...nētraṃ" etc., none of the MTG have it.

> ¹ṛṣir nāmnāṃ sahasrasya vēdavyāsō mahāmuniḥ  
> chandō’nuṣṭup tathā dēvō bhagavān dēvakīsutaḥ /14/  

¹ all; Cp D5 instead reads CE@631, whose L1 is "viṣṇōr nāmasahasrasya vēdavyāsō mahān ṛṣiḥ"

> amṛtāṃṡūdbhavō bījaṃ ṡaktir dēvakīnandanaḥ  
> ¹trisāmā hṛdayaṃ tasya ṡāntyarthē vini²yujyatē /15/  

¹ all; M1-3 "tridhāmā"  
² all; M1-3 "-yōjayēt"

In ¹, it must be "trisāmā" because all MTG agree about it in /62/.

After /15/, only T1 in. CE@633 as /16/

> viṣṇuṃ jiṣṇuṃ mahāviṣṇuṃ prabhaviṣṇuṃ mahēṡvaram  
> anēkarūpadaityāntaṃ namāmi puruṣōttamam /16/  

After /16/ begins the usual "dhyāna ṡlōkas" (kṣīrōdanvat pradēṡē ... ṡirasā caturbhujam),
however they are not inserted by even a single ms.

## Stōtra

> viṡvaṃ viṣṇur vaṣaṭkārō bhūtabhavyabhavatprabhuḥ  
> bhūtakṛd bhūtabhṛd bhāvō bhūtātmā bhūtabhāvanaḥ /1/  

> pūtātmā paramātmā ca muktānāṃ paramā gatiḥ  
> avyayaḥ puruṣaḥ sākṣī kṣētrajn̄ō’kṣara ēva ca /2/  

> yōgō ¹yōgavidāṃ nētā pradhānapuruṣēṡvaraḥ  
> ²nārasiṃhavapuḥ ṡrīmān kēṡavaḥ puruṣōttamaḥ /3/  

¹ all; M4 "yōgavatāṃ"  
² all; B6 Dn D6,7,9 "narasiṃha"

> sarvaḥ ṡarvaḥ ṡivaḥ sthāṇur bhūtādir nidhir avyayaḥ  
> sambhavō bhāvanō bhartā prabhavaḥ prabhur īṡvaraḥ /4/  

> svayambhūḥ ṡambhur ādityaḥ puṣkarākṣō mahāsvanaḥ  
> anādinidhanō dhātā vidhātā dhātur uttamaḥ /5/  

> apramēyō hṛṣīkēṡaḥ padmanābhō’maraprabhuḥ  
> viṡvakarmā manus tvaṣṭā sthaviṣṭhaḥ sthavirō dhruvaḥ /6/  

> agrāhyaḥ ṡāṡvataḥ kṛṣṇō lōhitākṣaḥ pratardanaḥ  
> prabhūtas ¹trikakubdhāma pavitraṃ maṅgaḻaṃ param /7/  

¹ all; Cp Cv B1,3,4 T2,3 G4 "trikakuddhāma"

> īṡānaḥ prāṇadaḥ prāṇō jyēṣṭhaḥ ṡrēṣṭhaḥ prajāpatiḥ  
> hiraṇyagarbhō bhūgarbhō mādhavō madhusūdanaḥ /8/  

> īṡvarō vikramī dhanvī mēdhāvī vikramaḥ kramaḥ  
> anuttamō durādharṣaḥ kṛtajn̄aḥ kṛtir ātmavān /9/  

> surēṡaḥ ṡaraṇaṃ ṡarma viṡvarētāḥ prajābhavaḥ  
> ahaḥ saṃvatsarō vyāḻaḥ pratyayaḥ sarvadarṡanaḥ /10/  

> ajaḥ sarvēṡvaraḥ siddhaḥ siddhiḥ sarvādir acyutaḥ  
> vṛṣākapir amēyātmā sarvayōgaviniḥsṛtaḥ /11/  

> vasur vasumanāḥ satyaḥ samātmā sammitaḥ samaḥ  
> amōghaḥ puṇḍarīkākṣō vṛṣakarmā vṛṣākṛtiḥ /12/  

> rudrō bahuṡirā babhrur viṡvayōniḥ ṡuciṡravāḥ  
> amṛtaḥ ṡāṡvataḥ sthāṇur varārōhō mahātapāḥ /13/  

> sarvagaḥ sarvavid bhānur viṣvaksēnō janārdanaḥ  
> vēdō vēdavid avyaṅgō vēdāṅgō vēdavit kaviḥ /14/  

> lōkādhyakṣaḥ surādhyakṣō dharmādhyakṣaḥ ¹kṛtākṛtaḥ  
> caturātmā caturvyūhaṡ caturdaṃṣṭraṡ caturbhujaḥ /15/  

¹ all; T G4 D6-9 "kṛtākṛtiḥ"

> bhrājiṣṇur bhōjanaṃ bhōktā sahiṣṇur jagadādijaḥ  
> anaghō vijayō jētā viṡvayōniḥ punarvasuḥ /16/  

> upēndrō vāmanaḥ prāṃṡur amōghaḥ ṡucir ūrjitaḥ  
> atīndraḥ saṅgrahaḥ sargō dhṛtātmā niyamō yamaḥ /17/  

> vēdyō vaidyaḥ sadāyōgī vīrahā mādhavō madhuḥ  
> atīndriyō mahāmāyō mahōtsāhō mahābalaḥ /18/  

> mahābuddhir mahāvīryō mahāṡaktir mahādyutiḥ  
> anirdēṡyavapuḥ ṡrīmān amēyātmā mahādri¹dhṛt /19/  

¹ M1-3 T1 Cp; et al. "-dhṛk"

> mahēṣvāsō mahībhartā ṡrīnivāsaḥ satāṃ gatiḥ  
> aniruddhaḥ ¹surānandō gōvindō gōvidāṃ patiḥ /20/  

¹ all; T D2,3,9 "sadānandō"

> marīcir damanō haṃsaḥ suparṇō bhujagōttamaḥ  
> hiraṇyanābhaḥ sutapāḥ padmanābhaḥ prajāpatiḥ /21/  

> amṛtyuḥ sarvadṛk siṃhaḥ sandhātā sandhimān sthiraḥ  
> ajō durmarṣaṇaḥ ṡāstā viṡrutātmā surārihā /22/  

> gurur gurutamō dhāma satyaḥ satyaparākramaḥ  
> nimiṣō’nimiṣaḥ sragvī vācaspatir udāradhīḥ /23/  

> agraṇīr grāmaṇīḥ ṡrīmān nyāyō nētā samīraṇaḥ  
> sahasramūrdhā viṡvātmā sahasrākṣaḥ sahasrapāt /24/  

> āvartanō nivṛttātmā saṃvṛtaḥ sampramardanaḥ  
> ahaḥ ¹saṃvartakō vahnir anilō dharaṇīdharaḥ /25/  

¹ all; M4 "saṃvatsarō"

> suprasādaḥ prasannātmā ¹viṡvadhṛg viṡvabhug vibhuḥ  
> satkartā satkṛtaḥ sādhur jahnur nārāyaṇō naraḥ /26/  

¹ all; Cp "viṡvasṛk"

> asaṅkhyēyō’pramēyātmā viṡiṣṭaḥ ṡiṣṭakṛc chuciḥ  
> siddhārthaḥ siddhasaṅkalpaḥ siddhidaḥ siddhisādhanaḥ /27/  

> vṛṣāhī vṛṣabhō viṣṇur vṛṣaparvā vṛṣōdaraḥ  
> vardhanō vardhamānaṡ ca viviktaḥ ṡrutisāgaraḥ /28/  

> subhujō durdharō vāgmī mahēndrō vasudō vasuḥ  
> naikarūpō bṛhadrūpaḥ ṡipiviṣṭaḥ prakāṡanaḥ /29/  

> ōjas tējō dyutidharaḥ prakāṡātmā pratāpanaḥ  
> ṛddhaḥ ¹spaṣṭākṣarō mantraṡ candrāṃṡur bhāskaradyutiḥ /30/  

¹ all; Cv "aṣtākṣarō"

> amṛtāṃṡūdbhavō bhānuḥ ¹ṡaṡabinduḥ surēṡvaraḥ  
> auṣadhaṃ jagataḥ sētuḥ satyadharmaparākramaḥ /31/  

¹ all (incl. Cp); D10 T2,3 G4 "ṡaṡi-"

> bhūtabhavyabhavannāthaḥ pavanaḥ pāvanō¹’nilaḥ  
> kāmahā kāmakṛt kāntaḥ kāmaḥ kāmapradaḥ prabhuḥ /32/  

¹ all; Cp "-’nalaḥ"

> yugādikṛd yugāvartō naikamāyō mahāṡanaḥ  
> adṛṡyō vyaktarūpaṡ ca sahasrajid anantajit /33/  

> iṣṭō viṡiṣṭaḥ ṡiṣṭēṣṭaḥ ṡikhaṇḍī nahuṣō vṛṣaḥ  
> krōdhahā krōdhakṛt kartā viṡvabāhur mahīdharaḥ /34/  

> acyutaḥ prathitaḥ prāṇaḥ prāṇadō vāsavānujaḥ  
> apāṃ nidhir adhiṣṭhānam apramattaḥ pratiṣṭhitaḥ /35/  

> skandaḥ skandadharō dhuryō varadō vāyuvāhanaḥ  
> vāsudēvō bṛhadbhānur ādidēvaḥ purandaraḥ /36/  

> aṡōkas tāraṇas tāraḥ ṡūraḥ ¹ṡaurir janēṡvaraḥ  
> anukūlaḥ ṡatāvartaḥ padmī padmanibhēkṣaṇaḥ /37/  

¹ see /69/ for repetition in some mss.

> padmanābhō’ravindākṣaḥ padmagarbhaḥ ṡarīrabhṛt  
> maharddhir ¹ṛddhō vṛddhātmā mahākṣō garuḍadhvajaḥ /38/  

¹ all (incl. CJ); M3 T2 G2 D4,5 "ruddhō"

> atulaḥ ṡarabhō bhīmaḥ samayajn̄ō havir hariḥ  
> sarvalakṣaṇalakṣaṇyō lakṣmīvān samitin̄jayaḥ /39/  

> vikṣarō rōhitō mārgō hētur dāmōdaraḥ sahaḥ  
> mahīdharō mahābhāgō vēgavān amitāṡanaḥ /40/  

> udbhavaḥ kṣōbhaṇō dēvaḥ ṡrīgarbhaḥ paramēṡvaraḥ  
> karaṇaṃ kāraṇaṃ kartā vikartā gahanō guhaḥ /41/  

> vyavasāyō vyavasthānaḥ saṃsthānaḥ sthānadō dhruvaḥ  
> pararddhiḥ paramaḥ spaṣṭas tuṣṭaḥ puṣṭaḥ ṡubhēkṣaṇaḥ /42/  

> rāmō virāmō ¹viratō mārgō nēyō nayō’nayaḥ  
> ²vīraḥ ṡaktimatāṃ ṡrēṣṭhō dharmō dharmavid uttamaḥ /43/  

¹ ?M T2-3 G1,3 Cp CE; M2 (marg.) Cp (alt.) T1 G2,4 B1,3 Dn D1,4,5,6,8,9 "virajō"  
² see /69/ for repetition in some mss.

> vaikuṇṭhaḥ puruṣaḥ prāṇaḥ prāṇadaḥ ¹praṇavaḥ pṛthuḥ  
> hiraṇyagarbhaḥ ṡatrughnō vyāptō vāyur adhōkṣajaḥ /44/  

¹ all; Cp "praṇamaḥ"; D1 "praṇabhuḥ"

> ¹ṛtuḥ sudarṡanaḥ kālaḥ paramēṣṭhī parigrahaḥ  
> ugraḥ saṃvatsarō dakṣō viṡrāmō viṡvadakṣiṇaḥ /45/  

¹ all; Cv "kratuḥ"

> vistāraḥ sthāvaraḥ sthāṇuḥ pramāṇaṃ bījam avyayam  
> arthō’narthō mahākōṡō mahābhōgō mahādhanaḥ /46/  

> anirviṇṇaḥ sthaviṣṭhō bhūr dharmayūpō mahāmakhaḥ  
> nakṣatranēmir nakṣatrī kṣamaḥ kṣāmaḥ samīhanaḥ /47/  

> yajn̄a ¹ijyō mahējyaṡ ca kratuḥ satraṃ satāṃ gatiḥ  
> sarvadarṡī ²vimuktātmā sarvajn̄ō jn̄ānam uttamam /48/  

¹ all; Dn "ījyō"  
² all; Cp "nivṛttātmā" (see /25/, /83/)

> suvrataḥ sumukhaḥ sūkṣmaḥ sughōṣaḥ sukhadaḥ suhṛt  
> manōharō jitakrōdhō vīrabāhur vidāraṇaḥ /49/  

> svāpanaḥ svavaṡō vyāpī naikātmā naikakarmakṛt  
> vatsarō vatsalō vatsī ratnagarbhō dhanēṡvaraḥ /50/  

> dharmagub dharmakṛd dharmī ¹sad asat kṣaram akṣaram  
> avijn̄ātā sahasrāṃṡur vidhātā kṛtalakṣaṇaḥ /51/  

¹ all; Cp "sad akṣaram asat kṣaram" (swap)

> gabhastinēmiḥ sattvasthaḥ siṃhō bhūtamahēṡvaraḥ  
> ādidēvō mahādēvō dēvēṡō dēvabhṛd guruḥ /52/  

> uttarō gōpatir gōptā jn̄ānagamyaḥ purātanaḥ  
> ṡarīrabhūtabhṛd bhōktā ¹kapīndrō bhūridakṣiṇaḥ /53/  

¹ all; M3 D1,2 "kavīndrō"

> sōmapō’mṛtapaḥ sōmaḥ purujit purusattamaḥ  
> vinayō jayaḥ satyasandhō dāṡārhaḥ sātvatāṃ patiḥ /54/  

> jīvō vinayitā sākṣī mukundō’mitavikramaḥ  
> ambhōnidhir anantātmā mahōdadhiṡayō’ntakaḥ /55/  

> ajō mahārhaḥ svābhāvyō jitāmitraḥ pramōdanaḥ  
> ānandō nandanō nandaḥ satyadharmā trivikramaḥ /56/  

> maharṣiḥ kapilācāryaḥ kṛtajn̄ō mēdinīpatiḥ  
> tripadas tridaṡādhyakṣō mahāṡṛṅgaḥ kṛtāntakṛt /57/  

> mahāvarāhō gōvindaḥ suṣēṇaḥ kanakāṅgadī  
> guhyō gabhīrō gahanō guptaṡ cakragadādharaḥ /58/  

> vēdhāḥ svāṅgō’jitaḥ kṛṣṇō dṛḍhaḥ saṅkarṣaṇō’cyutaḥ  
> varuṇō vāruṇō vṛkṣaḥ puṣkarākṣō mahāmanāḥ /59/  

> bhagavān bhagahā nandī vanamālī halāyudhaḥ  
> ādityō jyōtir ādityaḥ sahiṣṇur gatisattamaḥ /60/  

> sudhanvā khaṇḍaparaṡur dāruṇō draviṇapradaḥ  
> ¹divispṛk sarvadṛg vyāsō vācaspatir ayōnijaḥ /61/  

¹ M G T2,3 Cp; et al. "divaḥspṛk"

> trisāmā sāmagaḥ sāma nirvāṇaṃ bhēṣajaṃ bhiṣak  
> sannyāsakṛc chamaḥ ṡāntō niṣṭhā ṡāntiḥ ¹parāyaṇam /62/  

¹ all (incl. Cp); CJ B1,4,6 D4,5,6 "parāyaṇaḥ"

> ṡubhāṅgaḥ ṡāntidaḥ sraṣṭā kumudaḥ kuvalēṡayaḥ  
> gōhitō gōpatir gōptā vṛṣabhākṣō vṛṣapriyaḥ /63/  

> anivartī nivṛttātmā saṅkṣēptā kṣēmakṛc chivaḥ  
> ṡrīvatsavakṣāḥ ṡrīvāsaḥ ṡrīpatiḥ ṡrīmatāṃ varaḥ /64/  

> ṡrīdaḥ ṡrīṡaḥ ṡrīnivāsaḥ ṡrīnidhiḥ ṡrīvibhāvanaḥ  
> ṡrīdharaḥ ṡrīkaraḥ ṡrēyaḥ ṡrīmām̐l lōkatrayāṡrayaḥ /65/  

> svakṣaḥ svaṅgaḥ ṡatānandō nandir jyōtir gaṇēṡvaraḥ  
> vijitātmā vidhēyātmā satkīrtiṡ chinnasaṃṡayaḥ /66/  

> udīrṇaḥ sarvataṡcakṣur anīṡaḥ ṡāṡvataḥ sthiraḥ  
> bhūṡayō bhūṣaṇō bhūtir ¹viṡōkaḥ ṡōkanāṡanaḥ /67/  

¹ all (incl. Cp (alt.)); Cp "aṡōkaḥ"

> arciṣmān arcitaḥ kumbhō viṡuddhātmā viṡōdhanaḥ  
> aniruddhō’pratirathaḥ pradyumnō’mitavikramaḥ /68/  

> kālanēminihā ¹ṡauriḥ ṡūraḥ ²ṡūrajanēṡvaraḥ  
> trilōkātmā trilōkēṡaḥ kēṡavaḥ kēṡihā hariḥ /69/  

¹ ?M1,4 B Dn Cp (see /37/); M2,3 T G CE "vīraḥ" (see /43/, /70/)  
² M T2,3 Cp; et al. "ṡaurirjan-"

> kāmadēvaḥ kāmapālaḥ kāmī kāntaḥ kṛtāgamaḥ  
> anirdēṡyavapur viṣṇur vīrō’nantō dhanan̄jayaḥ /70/  

> brahmaṇyō brahmakṛd brahmā brahma brahmavivardhanaḥ  
> brahmavid brāhmaṇō brahmī brahmajn̄ō brāhmaṇapriyaḥ /71/  

> mahākramō mahākarmā mahātējā mahōragaḥ  
> mahākratur mahāyajvā mahāyajn̄ō mahāhaviḥ /72/  

> stavyaḥ stavapriyaḥ stōtraṃ ¹stutaḥ stōtā raṇapriyaḥ  
> pūrṇaḥ pūrayitā puṇyaḥ puṇyakīrtir anāmayaḥ /73/  

¹ M T2,3 G1,3,4 Cp; et al. "stutiḥ"

> manōjavas tīrthakarō vasurētā ¹vasupradaḥ  
> vasupradō vāsudēvō vasur vasumanā haviḥ /74/  

¹ all; M4 T1 G4 "vasupriyaḥ"

> sadgatiḥ satkṛtiḥ sattā sadbhūtiḥ ¹satparāyaṇaḥ  
> ṡūrasēnō yaduṡrēṣṭhaḥ sannivāsaḥ suyāmunaḥ /75/  

¹ all (incl. Cp (alt.)); D1 Cp "satparāyaṇam"

> bhūtāvāsō vāsudēvō sarvāsunilayō’nalaḥ  
> darpahā darpadō ¹dṛptō durdharō’thāparājitaḥ /76/  

¹ all (incl. Cp (alt.)); Cp "adṛptō"

> viṡvamūrtir mahāmūrtir dīptamūrtir amūrtimān  
> anēkamūrtir avyaktaḥ ṡatamūrtiḥ ṡatānanaḥ /77/  

> ēkō naikaḥ sa vaḥ kaḥ kiṃ yat tat padam anuttamam  
> lōkabandhur lōkanāthō mādhavō bhaktavatsalaḥ /78/  

> suvarṇavarṇō hēmāṅgō varāṅgaṡ candanāṅgadī  
> vīrahā viṣamaḥ ṡūnyō ghṛtāṡīr acalaṡ calaḥ /79/  

> amānī mānadō mānyō lōkasvāmī trilōka¹dhṛk  
> sumēdhā mēdhajō dhanyaḥ satyamēdhā dharādharaḥ /80/  

¹ M T2,3 G CE; T1 D1,2,8,9 "-kṛt"; Cp D10 B1,3 "-dhṛt"

> tējōvṛṣō dyutidharaḥ sarvaṡastrabhṛtāṃ varaḥ  
> pragrahō ¹nigrahō’vyagrō naikaṡṛṅgō gadāgrajaḥ /81/  

¹ all; Cp "nigrahō vyagrō"

> caturmūrtiṡ caturbāhuṡ caturvyūhaṡ caturgatiḥ  
> caturātmā caturbhāvaṡ caturvēdavid ēkapāt /82/  

> samāvartō nivṛttātmā durjayō duratikramaḥ  
> durlabhō durgamō durgō durāvāsō durārihā /83/  

> ṡubhāṅgō lōkasāraṅgaḥ sutantus tantuvardhanaḥ  
> indrakarmā mahākarmā kṛtakarmā kṛtāgamaḥ /84/  

> udbhavaḥ sundaraḥ sundō ratnanābhaḥ sulōcanaḥ  
> arkō ¹vājasanaḥ ṡṛṅgī jayantaḥ sarvavij jayī /85/  

¹ all; Cp Cv "vājasaniḥ"

> suvarṇabindur akṣōbhyaḥ sarvavāg īṡvarēṡvaraḥ  
> mahāhradō mahāgartō mahābhūtō mahānidhiḥ /86/  

> kumudaḥ kundaraḥ kundaḥ parjanyaḥ ¹pavanō’nilaḥ  
> ²amṛtāṡō’mṛtavapuḥ sarvajn̄aḥ sarvatōmukhaḥ /87/  

¹ M T2,3 G1,3 Cp CE; T1 G2,4 D4,8,9,10 Cv "pavanō’nalaḥ"; Dn "pā-"  
² M2,3 T2 G1,3,4 B1,2 D4,6,9 Cp Cv; M1,4 T1,3 CE "amṛtāṃṡō’-"

> sulabhaḥ suvrataḥ siddhaḥ ṡatrujic chatrutāpanaḥ  
> nyagrōdhōdumbarō’ṡvatthaṡ cāṇūrāndhraniṣūdanaḥ /88/  

> sahasrārciḥ saptajihvaḥ saptaidhāḥ saptavāhanaḥ  
> amūrtir anaghō’cintyō bhayakṛd bhayanāṡanaḥ /89/  

> aṇur bṛhat kṛṡaḥ sthūlō guṇabhṛn nirguṇō mahān  
> adhṛtaḥ svadhṛtaḥ ¹svāsyaḥ prāgvaṃṡō vaṃṡavardhanaḥ /90/  

¹ M T1 G2,3 Cp CE; T2,3 G1,4 D10 CJ "svāsthyaḥ"; Cv "svākhyaḥ"

> bhārabhṛt kathitō yōgī yōgīṡaḥ sarvakāmadaḥ  
> āṡramaḥ ṡramaṇaḥ kṣāmaḥ ¹suparṇō vāyuvāhanaḥ /91/  

¹ all; M3 D5 "suvarṇō"

> dhanurdharō dhanurvēdō daṇḍō ¹damayitā damaḥ  
> aparājitaḥ sarvasahō niyantā niyamō yamaḥ /92/  

¹ all; Cp "damayitā’damaḥ"

> sattvavān sāttvikaḥ satyaḥ satyadharmaparāyaṇaḥ  
> abhiprāyaḥ priyārhō’rhaḥ priyakṛt prītivardhanaḥ /93/  

> vihāyasagatir jyōtiḥ surucir hutabhug vibhuḥ  
> ravir ¹virōcanaḥ sūryaḥ savitā ravilōcanaḥ /94/  

¹ all (incl. Cp); T3 D1 "vilōcanaḥ"

> anantō hutabhug bhōktā sukhadō ¹naikajō’grajaḥ  
> anirviṇṇaḥ sadāmarṣī lōkādhiṣṭhānam ²adbhutam /95/  

¹ ?M1,2,3 Cv B Dn; M4 T G Cp CE "naikadō’-"  
² M T G Cv CE; B Dn Cp "adbhutaḥ"

> sanāt sanātanatamaḥ ¹kapilaḥ kapir avyayaḥ  
> svastidaḥ svastikṛt svasti svastibhuk svastidakṣiṇaḥ /96/  

¹ all; Cv "kapilaḥ kapilōvyayaḥ"

> araudraḥ kuṇḍalī cakrī vikramy ūrjitaṡāsanaḥ  
> ṡabdātigaḥ ṡabdasahaḥ ṡiṡiraḥ ṡarvarīkaraḥ /97/  

> akrūraḥ pēṡalō dakṣō dakṣiṇaḥ kṣamiṇāṃ varaḥ  
> vidvattamō vītabhayaḥ puṇyaṡravaṇakīrtanaḥ /98/  

> uttāraṇō duṣkṛtihā puṇyō duḥsvapnanāṡanaḥ  
> vīrahā rakṣaṇaḥ santō jīvanaḥ paryavasthitaḥ /99/  

> anantarūpō’nantaṡrīr jitamanyur bhayāpahaḥ  
> caturaṡrō gabhīrātmā vidiṡō vyādiṡō diṡaḥ /100/  

> anādir bhūr bhuvō lakṣmīḥ suvīrō rucirāṅgadaḥ  
> jananō janajanmādir bhīmō bhīmaparākramaḥ /101/  

> ādhāranilayō dhātā puṣpahāsaḥ prajāgaraḥ  
> ūrdhvagaḥ satpathācāraḥ prāṇadaḥ praṇavaḥ paṇaḥ /102/  

> pramāṇaṃ prāṇanilayaḥ prāṇa¹kṛt prāṇajīvanaḥ  
> tattvaṃ tattvavid ēkātmā janmamṛtyujarātigaḥ /103/  

¹ M G1 Cv CE; T G2,3 "-dhṛk"; G4 Cp "-dhṛt"

> bhūr bhuvaḥ svas tarus tāraḥ savitā prapitāmahaḥ  
> yajn̄ō yajn̄apatir yajvā yajn̄āṅgō yajn̄avāhanaḥ /104/  

> yajn̄abhṛd yajn̄akṛd yajn̄ī yajn̄abhug yajn̄asādhanaḥ  
> yajn̄āntakṛd yajn̄aguhyam annam annāda ēva ca /105/  

> ātmayōniḥ svayan̄jātō vaikhānaḥ sāmagāyanaḥ  
> dēvakīnandanaḥ sraṣṭā kṣitīṡaḥ pāpanāṡanaḥ /106/  

> ṡaṅkhabhṛn nandakī cakrī ṡārṅgadhanvā gadādharaḥ  
> rathāṅgapāṇir akṣōbhyaḥ sarvapraharaṇāyudhaḥ /107/  
> ¹sarvapraharaṇāyudhaḥ ōṃ nama iti  

¹ M2,3 T G D5,10; et al. om.

None of the mss. in. "vanamālī gadī ṡārṅgī..." after /107/.

> vanamālī gadī ṡārṅgī ṡaṅkhī cakrī ca nandakī  
> ṡrīmān nārāyaṇō viṣṇur vāsudēvō’bhirakṣatu /108/  

## Phala-ṡruti

> itīdaṃ kīrtanīyasya kēṡavasya mahātmanaḥ  
> nāmnāṃ sahasraṃ divyānām aṡēṣēṇa prakīrtitam /1/  

> ya idaṃ ṡṛṇuyān nityaṃ yaṡ cāpi parikīrtayēt  
> nāṡubhaṃ prāpnuyāt kin̄cit sō’mutrēha ca mānavaḥ /2/  

> vēdāntagō brāhmaṇaḥ syāt kṣatriyō vijayī bhavēt  
> vaiṡyō dhanasamṛddhaḥ syāc chūdraḥ sukham avāpnuyāt /3/  

> dharmārthī prāpnuyād dharmam arthārthī cārtham āpnuyāt  
> kāmān avāpnuyāt kāmī prajārthī ¹cāpnuyāt prajāḥ /4/  

¹ ?M2,3 T2,3 G3,4 Cp Cv CE; M1,4 T1 G1 Dn D1,2,3 "prāpnuyāt-"; G2 M2 (marg.) D4,5,6,7,9 "-prajām"

> bhaktimān yaḥ sadōtthāya ṡucis tadgatamānasaḥ  
> sahasraṃ vāsudēvasya nāmnām ētat prakīrtayēt /5/  

> yaṡaḥ prāpnōti vipulaṃ ¹jn̄ātiprādhānyam ēva ca  
> acalāṃ ṡriyam āpnōti ṡrēyaṡ ²cāpnōty anuttamam /6/  

¹ M T1 G1,2,3 Cp CE; T2,3 G4 Cp (alt.) CJ "yāti-"  
² M T3 G2,3,4 Cv CE; T1,2 G1 Cp "prāpnōty-"

> na bhayaṃ kvacid āpnōti vīryaṃ tējaṡ ca vindati  
> bhavaty arōgō dyutimān balarūpaguṇānvitaḥ /7/  

> rōgārtō mucyatē rōgād baddhō mucyēta bandhanāt  
> bhayān mucyēta bhītas ¹tu ²mucyētāpanna āpadaḥ /8/  

¹ M1,2,3 T2,3 G3,4 Cp; M4 T1 G1,2 B Dn CE "ca"  
² ?M2,3 T1,2 G3 Cp Cv CE; M1,4 T3 G1,2,4 "mucyēd-"

> durgāṇy atitaraty āṡu puruṣaḥ puruṣōttamam  
> stuvan nāmasahasrēṇa nityaṃ bhaktisamanvitaḥ /9/  

> vāsudēvāṡrayō martyō vāsudēvaparāyaṇaḥ  
> sarvapāpaviṡuddhātmā yāti brahma sanātanam /10/  

> na vāsudēvabhaktānām aṡubhaṃ vidyatē kvacit  
> janmamṛtyujarāvyādhibhayaṃ ¹vā’py upajāyatē /11/  

¹ M T G Cv Cp CE; CJ B5 Dn "naivōpajāyatē"; D1,2,7 Cp (alt.) "nāpy-"

> imaṃ stavam adhīyānaḥ ṡraddhābhaktisamanvitaḥ  
> yujyētātmasukhakṣāntiṡrīdhṛtismṛtikīrtibhiḥ /12/  

> na krōdhō na ca mātsaryaṃ na lōbhō nāṡubhā matiḥ  
> bhavanti kṛtapuṇyānāṃ bhaktānāṃ puruṣōttamē /13/  

> dyauḥ sacandrārka¹nakṣatrā khaṃ diṡō bhūr mahōdadhiḥ  
> vāsudēvasya vīryēṇa vidhṛtāni mahātmanaḥ /14/  

¹ all; T1 Cp "-nakṣatraṃ"

> sasurāsuragandharvaṃ sayakṣōragarākṣasam  
> jagad vaṡē vartatēdaṃ kṛṣṇasya sacarācaram /15/  

> indriyāṇi manō buddhiḥ sattvaṃ tējō balaṃ dhṛtiḥ  
> vāsudēvātmakāny āhuḥ kṣētraṃ ¹kṣētrajn̄a ēva ca /16/  

¹ M1,2,3 T1 G1 Cp CE; M4 T2,3 G2,3,4 "kṣētrajn̄am"

> sarvāgamānām ācāraḥ prathamaṃ pari¹kalpyatē  
> ācāraprabhavō dharmō dharmasya prabhur acyutaḥ /17/  

¹ M T1,2 G1,2,3 CE; T3 G4 "-kalpatē"; Cp "-kalpitaḥ"

> ṛṣayaḥ pitarō dēvā mahābhūtāni dhātavaḥ  
> jaṅgamājaṅgamaṃ cēdaṃ jagan nārāyaṇōdbhavam /18/  

> yōgō jn̄ānaṃ tathā sāṅkhyaṃ vidyāḥ ¹ṡilpādi karma ca  
> vēdāḥ ṡāstrāṇi vijn̄ānam ētat sarvaṃ janārdanāt /19/  

¹ M T G Cp; B D CE "ṡilpāni"

> ēkō viṣṇur mahad bhūtaṃ pṛthag bhūtāny anēkaṡaḥ  
> trīm̐l lōkān vyāpya bhūtātmā bhuṅktē viṡvabhug avyayaḥ /20/  

> imaṃ stavaṃ bhagavatō viṣṇōr vyāsēna kīrtitam  
> paṭhēd ya icchēt puruṣaḥ ṡrēyaḥ prāptuṃ sukhāni ca /21/  

> viṡvēṡvaram ajaṃ dēvaṃ jagataḥ ¹prabhavāpyayam  
> bhajanti yē puṣkarākṣaṃ na tē yānti parābhavam /22/  
> na tē yānti parābhavam ōṃ nama iti  

¹ M T1 G2,4 Cp Cv CE; T2,3 "prabhur avyayam"; G1,3 CJ "prabhum avyayam"

Here ends chapter 13.135 in CE.

T G Dn CJ in. CE@635 as /23/-/25/ but M CE om.

>    arjuna uvāca  
> padmapatraviṡālākṣa padmanābha surōttama  
> bhaktānām anuraktānāṃ trātā bhava janārdana /23/  

>    ṡrībhagavān uvāca  
> yō māṃ nāmasahasrēṇa stōtum icchati pāṇḍava  
> sō’ham ēkēna ṡlōkēna stuta ēva na saṃṡayaḥ /24/  
> stuta ēva na saṃṡaya ōṃ nama iti  

After /24/, T2,3 G D5 CJ in. CE@636

>    vyāsa uvāca  
> vāsanād vāsudēvasya vāsitaṃ tē jagattrayam  
> sarvabhūtanivāsō’si vāsudēva namō’stu tē /25/  
> vāsudēva namō’stuta ōṃ nama iti  

T G CJ cont. and M1,2,3 in. /26/ after /22/ (Dn in. after /24/, skipping /25/)

>    brahmōvāca  
> namō’stv anantāya sahasramūrtayē  
>  sahasrapādākṣiṡirōrubāhavē  
> sahasranāmnē puruṣāya ṡāṡvatē  
>  sahasrakōṭīyugadhāriṇē namaḥ /26/  
> sahasrakōṭīyugadhāriṇa ōṃ nama iti  

After /26/, only M1-3 cont. CE@639 as /27/-/29/ and CE@640 as /30/

> namō brahmaṇyadēvāya gōbrāhmaṇahitāya ca  
> jagaddhitāya kṛṣṇāya gōvindāya namō namaḥ /27/  

> ākāṡāt patitaṃ tōyaṃ yathā gacchati sāgaram  
> sarvadēvanamaskāraḥ kēṡavaṃ prati gacchati /28/  

> sarvavēdēṣu yat puṇyaṃ sarvavēdēṣu yat phalam  
> tat phalaṃ puruṣa āpnōti stutvā dēvaṃ janārdanam /29/  

> jitaṃ tē puṇḍarīkākṣa namas tē viṡvabhāvana  
> namas tē’stu hṛṣīkēṡa mahāpuruṣapūrvaja /30/  

Thus ends chapter T1 227; G1,3 229; M1,2 231; M3 232; T2,3 G2,4 235; M4 237

After /29/ most Northern mss. B Dn D1,2,4-9 in. App-I,No.18 (177 lines)

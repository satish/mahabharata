# Ādi 19 Samudra Varṇanam "Description of the ocean"

A critical edition of Samudra Varṇanam based chiefly on Malayalam mss. and southern (T G) recension.

M1-5 – Malayalam
T1-2 – Telugu
G1-6 – Grantha

>    sūtaḥ –  
> tatō ¹rātryāṃ vyatītāyāṃ prabhāta uditē ravau  
> kadrūṡ ca vinatā caiva bhaginyau tē tapōdhana /1/  

¹ M T G (except M1 G6); M1 G6 CE Dn "rajanyāṃ vyuṣṭāyāṃ"

> amarṣitē ¹susaṃrabdhē dāsyē kṛtapaṇē tadā  
> jagmatus turagaṃ draṣṭum uccaiḥṡravasam antikāt /2/  

¹ all; M (except M1) "tu saṃrabdhē"

> dadṛṡātē tadā tatra samudraṃ nidhim ambhasām  
> timiṃgilajhaṣākīrṇaṃ makarair āvṛtaṃ ¹tathā /3/  

¹ all; M1 T1 G4,5 "tadā"

> sattvaiṡ ca bahusāhasrair nānārūpaiḥ ¹samāvṛtam  
> ugrair nityam anādhṛṣyaṃ kūrmagrāhasamākulam /4/  

¹ all; M2,4,5 "apāghṛtam"

Only M1 omits verses 5 – 7. Others (incl. CE) don't.  
M5 omits verse 6cd.

> ākaraṃ sarvaratnānām ālayaṃ varuṇasya ca  
> nāgānām ālayaṃ ramyam uttamaṃ saritāṃ patim /5/  

> pātālajvalanāvāsam asurāṇāṃ ca bandhanam  
> bhayaṃkaraṃ ca sattvānāṃ payasāṃ nidhim ¹adbhutam /6/  

¹ M2,3,4 T G PPS; CE "arṇavam"

> ṡubhaṃ divyam amartyānām amṛtasyākaraṃ ¹param  
> apramēyam acintyaṃ ca supuṇyajalam adbhutam /7/  

¹ all; B Da M3,4 "varam"

> ghōraṃ jalacarā¹kīrṇaṃ raudraṃ bhairavanisvanam  
> gambhīrāvarta²salilaṃ sarvabhūtabhayaṃkaram /8/  

¹ M T G PPS; CE "-rāvaraudraṃ"  
² M T G PPS; CE "-kalilaṃ"

> ¹vēlālōlānilabalaṃ ²kṣōbhavēgasamutthitaiḥ  
> vīcīhastaiḥ ³prasaritaiḥ nṛtyantam iva ⁴sarvataḥ /9/  

¹ M T G; CE "vēlādōlānilacalaṃ"  
² M T G; CE "kṣōbhōdvēgasamutthitam"  
³ M G2,3; et al. "pracalitair"  
⁴ M T G; CE "sarvaṡaḥ"

> candravṛddhikṣayavaṡād ¹udvṛttōdaṃ durāsadam  
> pān̄cajanyasya ²jananaṃ ratnākaram anuttamam /10/  

¹ ? M2-5; M1 G "uddhṛtōrmidurāsadam"; CE "udvṛttōrmidurāsadam"; PPS "utthitōrmiṃ durāsadam"; T1 "xxx"; T2 "yyy"  
² all; M2-5 G2,3 "janakaṃ"

> gāṃ vindatā bhagavatā gōvindēnāmitaujasā  
> varāharūpiṇā cāntar vikṣōbhitajalāvilam /11/  

> brahmarṣiṇā ca tapatā varṣāṇāṃ ṡatam atriṇā  
> ¹anāsāditagādhaṃ ca pātālatalam avyayam /12/  

¹ all; T G2,3 "anāsādyamagādhaṃ"

> adhyātmayōganidrāṃ ca padmanābhasya sēvataḥ  
> ¹yugādikālē ṡayanaṃ viṣṇōr amitatējasaḥ /13/  

¹ M; T CE "yugādikāla-"; G1,4-6 PPS "yugāntakāla-"; G2 "yugādikālaṃ ṡa-"

> vaḍavāmukhadīptāgnēs tōyahavyapradaṃ ṡubham  
> ¹agādhapāravistīrṇam apramēyaṃ saritpatim /14/  

¹ M G3; T G1,4-6 PPS "agādhatalavi-"; G2 "agādhamapi vi-"; CE "agādhapāraṃ vi-"

> mahānadībhir bahvībhiḥ spardhayēva sahasraṡaḥ  
> abhisāryamāṇam aniṡaṃ ¹tatra tatra samantataḥ /15/  

¹ M2-5 G PPS; M1 T "tatra tatra mahārṇavam"; CE "dadṛṡātē mahārṇavam"

> gambhīraṃ timimakarōgrasaṅkulaṃ taṃ  
>  garjantaṃ jalacara¹rāvanāditais taiḥ  
> vistīrṇaṃ dadṛṡatur ambaraprakāṡaṃ  
>  tē’gādhaṃ nidhim urum ambhasām ²apāram /16/  

¹ M T G PPS; CE "-rāvaraudranādaiḥ"  
² M T G PPS; CE "anantam"

> ity ēvaṃ jhaṣamakarōrmi-saṅkulaṃ taṃ  
>  gambhīraṃ vikasitam ambaraprakāṡam  
> pātāla-jvalana-ṡikhāvidīpitaṃ taṃ  
>  paṡyantyau drutam abhipētatus tadānīm /17/  
